﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace task1_num5a_vocabulary
{
    public class Dictionary
    {
        // словарь
        private readonly List<string> _dict;

        // свойство для чтения, возвращает словарь
        public List<string> Dict { get { return _dict; } }
        // свойство для чтения, показывающее количество элементов хеш-таблицы
        public int Count => _dict.Count;

        // конструктор
        public Dictionary()
        {
            _dict = new List<string>();
        }

        // добавление слова
        public bool Add(string word)
        {
            if (_dict.Contains(word))
                return false;
            _dict.Add(word);
            return true;
        }

        // удаление слова
        public bool Delete(string word)
        {
            if (!_dict.Contains(word))
                return false;
            _dict.Remove(word);
            return true;
        }

        // очистка словаря
        public void Clear()
        {
            _dict.Clear();
        }

        // сохранение словаря в файл
        public void SaveToFile(string fileName)
        {
            StreamWriter sw = new StreamWriter(fileName);
            foreach (string word in _dict)
                sw.WriteLine(word);
            sw.Close();
        }

        // загрузка словаря из файла
        public bool LoadFromFile(string fileName)
        {
            StreamReader sr = new StreamReader(fileName);
            bool result = true;
            while (sr.Peek() >= 0 && result)
            {
                string word = sr.ReadLine().Trim();
                result = Add(word);
            }
            if (!result)
                Clear();
            sr.Close();
            return result;
        }

        // печать словаря на textbox
        public void PrintToTextBox(TextBox tb)
        {
            tb.Text = "";
            foreach (string word in _dict)
                tb.AppendText(word + '\n');
        }
    }
}
