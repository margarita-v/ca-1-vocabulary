﻿using System.Windows.Forms;

namespace task1_num5a_vocabulary
{
    public class DictGUI : Dictionary
    {
        private string _fileName;
        private bool _modified;
        private readonly TextBox _textBox;

        public DictGUI(TextBox tb)
        {
            _fileName = "";
            _modified = false;
            _textBox = tb;
            PrintToTextBox(_textBox);
        }

        // добавление слова
        public new void Add(string word) 
        {
            if (base.Add(word))
                Modified = true;
        }

        // удаление слова
        public new void Delete(string word) 
        {
            if (base.Delete(word))
                Modified = true;
        }

        public new void Clear()
        {
            base.Clear();
            Modified = true;
        }

        // сохранение словаря в файл
        public new void SaveToFile(string name) 
        {
            _fileName = name;
            base.SaveToFile(name);
            Modified = false;
        }

        // загрузка словаря из файла
        public new bool LoadFromFile(string name) 
        {
            _fileName = name;
            bool res = base.LoadFromFile(_fileName);
            PrintToTextBox(_textBox);
            Modified = false;
            return res;
        }

        // свойство, показывающее, был ли словарь изменен
        public bool Modified 
        { 
            get { return _modified; }
            set
            {
                _modified = value;
                if (value)
                    PrintToTextBox(_textBox);
            }
        }

        // свойство для чтения, возвращает имя файла
        public string FileName { get { return _fileName; } }
    }
}
