﻿using System;
using System.IO;
using System.Windows.Forms;

namespace task1_num5a_vocabulary
{
    /* Даны слово и словарь, представляющий собой набор слов. Подсчитать количество слов в словаре, которые можно составить 
     * из букв исходного слова (разрешается использовать не все буквы. */

    public partial class FormMain : Form
    {
        private DictGUI _dict;

        public FormMain()
        {
            InitializeComponent();
            fdSave.InitialDirectory = Directory.GetCurrentDirectory();
            fdOpen.InitialDirectory = fdSave.InitialDirectory;
            Application.Idle += MainIdle;
        }

        private void MainIdle(object sender, EventArgs e)
        {
            lblSourceWord.Visible = lblWord.Visible = 
                lblDict.Visible = lblWords.Visible = tbInput.Visible = tbResult.Visible = 
                addToolStripMenuItem.Enabled = saveAsToolStripMenuItem.Enabled =
                saveToolStripMenuItem.Enabled = closeToolStripMenuItem.Enabled = _dict != null;

            deleteToolStripMenuItem.Enabled = 
                runToolStripMenuItem.Enabled = clearToolStripMenuItem.Enabled = _dict != null && _dict.Count != 0;
        }

        // создание нового файла
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WasModified(_dict, fdSave);
            _dict = new DictGUI(tbInput);
        }

        // открытие существующего файла
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WasModified(_dict, fdSave);
            fdOpen.FileName = "";
            if (fdOpen.ShowDialog() == DialogResult.OK)
            {
                _dict = new DictGUI(tbInput);
                if (!_dict.LoadFromFile(fdOpen.FileName))
                    MessageBox.Show(@"File loading error");
            }
        }

        // сохранение файла под тем же именем
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveDictionary(_dict, fdSave);
        }

        // сохранение файла под другим именем
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fdSave.FileName = _dict.FileName;
            if (fdSave.ShowDialog() == DialogResult.OK)
                _dict.SaveToFile(fdSave.FileName);
        }

        // закрытие файла
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WasModified(_dict, fdSave);
            _dict = null;
        }

        // выход из программы
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        // добавление слова
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInput dialog = new FormInput("Add");
            if (dialog.ShowDialog() == DialogResult.OK)
                _dict.Add(dialog.Word);
        }

        // удаление слова
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInput dialog = new FormInput("Delete");
            if (dialog.ShowDialog() == DialogResult.OK)
                _dict.Delete(dialog.Word);
        }

        // очистка словаря
        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_dict == null)
                return;
            DialogResult dialogResult = MessageBox.Show(@"Do you really want to clear dictionary?", "", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                _dict.Clear();
        }

        // решение задачи
        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormInput dialog = new FormInput("Task");
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                lblWord.Text = dialog.Word;
                tbResult.Text = "";
                Algorithm solve = new Algorithm(dialog.Word);
                var result = solve.GetSolve(_dict.Dict);
                if (result.Count == 0)
                    MessageBox.Show(@"Dictionary hasn't searched words.");
                else
                    foreach (var word in result)
                        tbResult.AppendText(word + '\n');
            }
        }

        // сохранение словаря в файл
        private static void SaveDictionary(DictGUI dict, SaveFileDialog dialog)
        {
            if (dict == null)
            {
                MessageBox.Show(@"Dictionary is empty");
                return;
            }
            if (dict.FileName == "")
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                    dict.SaveToFile(dialog.FileName);
            }
            else
                dict.SaveToFile(dict.FileName);
        }

        // функция проверки, был ли изменен словарь
        private static void WasModified(DictGUI dict, SaveFileDialog dialog)
        {
            if (dict != null && dict.Modified)
            {
                DialogResult dl = MessageBox.Show(@"Save changes?", "", MessageBoxButtons.YesNo);
                if (dl == DialogResult.Yes)
                    SaveDictionary(dict, dialog);
            }
        }

        // событие при закрытии формы
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dl = MessageBox.Show(@"Do you really want to close window?", "", MessageBoxButtons.YesNo);
            if (dl == DialogResult.Yes)
                WasModified(_dict, fdSave);
            else
                e.Cancel = true;
        }

        private void tbInput_TextChanged(object sender, EventArgs e)
        {
            lblWord.Text = @"not chosen";
            tbResult.Text = "";
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"Даны слово и словарь, представляющий собой набор слов. Подсчитать количество слов в словаре, которые можно составить из букв исходного слова (разрешается использовать не все буквы.",
                @"Task condition");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"Володина Маргарита, 3 курс 9 группа", @"About developers");
        } 
    }
}
