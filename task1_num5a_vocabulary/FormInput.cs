﻿using System.Windows.Forms;

namespace task1_num5a_vocabulary
{
    public partial class FormInput : Form
    {
        public string Word;
        public FormInput(string title)
        {
            InitializeComponent();
            Text = title;
        }

        public sealed override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        private void btnOk_Click(object sender, System.EventArgs e)
        {
            Word = tbInput.Text.Trim();
            if (Word.Length != 0)
                DialogResult = DialogResult.OK;
            else
                MessageBox.Show(@"Invalid input!");
        }
    }
}
