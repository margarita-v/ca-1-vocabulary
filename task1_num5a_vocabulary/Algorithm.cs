﻿using System;
using System.Collections.Generic;

namespace task1_num5a_vocabulary
{
    public class Algorithm
    {
        // исходное слово; из его букв будут составлены размещения без повторений
        private readonly string _sourceWord;

        public Algorithm(string word)
        {
            _sourceWord = word;
        }

        // функция меняет местами элементы с заданными индексами в массиве а
        private void Swap(ref int[] a, int i, int j)
        {
            int x = a[i];
            a[i] = a[j];
            a[j] = x;
        }

        // получение следующего размещения; n предметов, m мест
        private bool NextPerm(ref int[] perm, int n, int m)
        {
            int edge = m - 1;

            int j = m;
            while (j < n && perm[edge] >= perm[j])
                ++j;

            if (j < n)
                Swap(ref perm, edge, j);
            else
            {
                // меняем местами элементы с m по n-1
                for (int left = m, right = n - 1; left < right; left++, right--)
                    Swap(ref perm, left, right);

                int i = edge - 1;
                while (i >= 0 && perm[i] >= perm[i + 1])
                    --i;

                // последнее размещение
                if (i < 0)
                    return false;

                j = n - 1;
                while (j > i && perm[i] >= perm[j])
                    --j;

                Swap(ref perm, i, j);

                // меняем местами элементы с i+1 по n-1
                for (int left = i + 1, right = n - 1; left < right; left++, right--)
                    Swap(ref perm, left, right);
            }
            return true;
        }

        // получение слова с помощью массива perm, элементы которого соответствуют индексам букв из массива source
        string GetWord(char[] source, int[] perm, int size)
        {
            string result = "";
            for (int i = 0; i < size; i++)
                result += source[perm[i] - 1];
            return result;
        }

        // получение всех размещений без повторений из букв исходного слова
        public List<string> GetSolve(List<string> dict)
        {
            List<string> result = new List<string>();
            int len = _sourceWord.Length;
            for (int i = 1; i <= len; i++)
            {
                // сортируем буквы слова, так как все размещения будут получены в лексикографическом порядке
                char[] wordArr = _sourceWord.ToCharArray();
                Array.Sort(wordArr);

                // массив номеров букв; перестановки элементов массива соотвествуют перестановкам букв слова по соотв. индексам
                int[] perm = new int[wordArr.Length];
                for (int j = 0; j < perm.Length; j++)
                    perm[j] = j + 1;

                while (NextPerm(ref perm, len, i))
                {
                    string word = GetWord(wordArr, perm, i);
                    if (dict.Contains(word) && !result.Contains(word))
                        result.Add(word);
                }
            }
            return result;
        }
    }
}
